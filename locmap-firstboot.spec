Name:       locmap-firstboot
Version:    2.1
Release:    5%{?dist}
Summary:    Firstboot package for Locmap

Group:      CERN/Utilities
License:    BSD
URL:        http://linux.cern.ch
Source0:    %{name}-%{version}.tar.gz

BuildArch:  noarch

BuildRequires:  systemd
Requires: systemd

%description
Package for first boot time, responsible for configuring afs and other selected modules via locmap.

%prep
%setup

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/firstboot/modules/
install -d %{buildroot}/%{_sysconfdir}/sysconfig/
install -d %{buildroot}/%{_sbindir}
install -d %{buildroot}/%{_unitdir}


install -m 0644 locmap-firstboot-graphical.service %{buildroot}/%{_unitdir}/
install -m 0751 locmap-firstboot %{buildroot}/%{_sbindir}
install -m 0644 etc/sysconfig/locmap-firstboot %{buildroot}/%{_sysconfdir}/sysconfig/

%post
%if 0%{?systemd_post:1}
  %systemd_post locmap-firstboot-graphical.service
  # HACK: the above macro doesn't actually do the right thing (at anaconda install time), so ...
  [ -d /etc/systemd/system/graphical.target.wants ] && /bin/ln -s /usr/lib/systemd/system/locmap-firstboot-graphical.service /etc/systemd/system/graphical.target.wants/locmap-firstboot-graphical.service >/dev/null 2>&1 || :

%else
  if [ $1 -eq 1 ] ; then
    # Initial installation
    /usr/bin/systemctl enable locmap-firstboot-graphical.service >/dev/null 2>&1 || :
    /bin/rm -f /usr/share/firstboot/modules/rhsm_login.py* || :
    # HACK: we have it only in gui mode, but on 7.2 tui mode may start bypassing us ..
    #
    /usr/bin/systemctl disable initial-setup-text.service >/dev/null 2>&1 || :
  fi
%endif

%preun
%if 0%{?systemd_preun:1}
  %systemd_preun locmap-firstboot-graphical.service
%else
  if [ $1 = 0 ]; then
    /bin/systemctl --no-reload disable locmap-firstboot-graphical.service >/dev/null 2>&1 || :
    /bin/systemctl stop locmap-firstboot-graphical.service >/dev/null 2>&1 || :
  fi
%endif

%postun
%if 0%{?systemd_postun:1}
  %systemd_postun locmap-firstboot-graphical.service
%else
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /usr/bin/systemctl try-restart locmap-firstboot-graphical.service >/dev/null 2>&1 || :
fi
%endif


%files
%defattr(-,root,root)
%{_sbindir}/locmap-firstboot
%config %{_unitdir}/locmap-firstboot-graphical.service
%config(noreplace) %{_sysconfdir}/sysconfig/locmap-firstboot
%doc README.md


%changelog
* Wed May 20 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-5
- fix logic on systems where no GUI is installed

* Mon Apr 20 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-4
- add fix for systems where no GUI is installed

* Thu Feb 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-3
- move to systemd macros for EL8
- add workaround for service enablement during anaconda

* Tue Feb 25 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- remove locmap Requires as it comes from a different source

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- build for C8

* Fri May 11 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-2
- Fix logging for firstboot locmap v2.0 in CC7.5

* Mon Sep 11 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-2
- Redone for initial-setup plugin

* Mon Jan 09 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-8
- disable nscd by default
- fix typo on path

* Fri Dec 16 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-6
- fixing wrong copy / paste for unit name...
- enabling locmap-firstboot in sysconfig

* Fri Dec 16 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.3-4
- improve post scripts, systemd unit.

* Wed Dec 14 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3
- Update Makefile

* Tue Nov 1 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Add the lpadmin module to default list modules

* Wed Sep 7 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Adding eosclient and cvmfs features

* Fri Sep 2 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Fixing systemd service for installation time

* Fri Jun 24 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
